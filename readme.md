# Slide to unlock 

A spam fighting trick that is easy to implement and easy to use.

## dependencies

`jquery 2.0`

## Installation

```html
<div id="slider-container" class="no-select invisible">
    <div id="slider-bar">
        <span>Slide To Complete</span>
        <div class="handle"><i class="fa fa-chevron-right"></i></div>
    </div>
</div>
```

```css
#slider-container { 
    width: 100%;
    margin: 20px auto; 
    height: 60px;

}
#slider-bar {
    position: relative;
    height: 100%;
    width: 100%;
    background: #ccc;
    border-radius: 6px;
    overflow: hidden;
}
.handle {
    background: #3399ff;
    width: 80px;
    height: 100%;
    line-height: 60px;
    display: inline-block;
    vertical-align: middle;
    line-height: 1;
    border-radius: 6px;
    cursor: pointer;
    text-align: center;
    position: absolute;
    top: 0;
    left: 0;
}
.handle i {
    font-size: 3rem;
    color: #fff;
    line-height: 60px;
    vertical-align: middle;
}
#slider-bar span {
    position: absolute;
    text-align: center;
    width: 60%;
    height: 60px;
    line-height: 60px;
    padding-left: 5px;
    -webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none; user-select: none;
    right: 0;
    left: 0;
    margin: auto;
}
```

```js
$(function(){

    slidebar_width = $('#slider-bar').outerWidth();
    handle_width = $('#slider-bar .handle').outerWidth();
    $('#slider-bar').scrollbar({
        onDrag:function(x){
            if( (x + handle_width) >= (slidebar_width-5) ) {
                $('#slider-bar').delay(100).fadeOut(200);
                return false;
            }
        },
        onDrop:function(x){
            if( (x + handle_width) < (slidebar_width-5) ) {
                $('.handle').animate({'left' : 0 }, 200 );
            }else{
                alert('thank you, you are not spam :)');
            }
        }
    });

});
```

## Usage

Install the code above, run it in a browser. You will see a slide button. drag it to the right and done. Most spam can't do that. Cool way to avoid some spams and make your user happy.

